<?php
/**
 * @file
 * Defines the View Style Plugins for Views StoryMapJS module.
 */

/**
 * Implements hook_views_plugins().
 */
function views_storymapjs_views_plugins() {
  return array(
    'style' => array(
      'storymapjs' => array(
        'title' => t('StoryMapJS'),
        'help' => t('Display the results in a StoryMap.'),
        'handler' => 'views_storymapjs_plugin_style_storymapjs',
        'uses options' => TRUE,
        'uses row plugin' => FALSE,
        'uses grouping' => FALSE,
        'uses fields'     => TRUE,
        'uses row class' => FALSE,
        'type' => 'normal',
        'path' => drupal_get_path('module', 'views_storymapjs'),
        'theme' => 'views_storymapjs',
        'theme path' => drupal_get_path('module', 'views_storymapjs') . '/theme',
        'theme file' => 'views_storymapjs.theme.inc',
      ),
    ),
  );
}
