<?php
/**
 * @file
 * Theme preprocessors.
 */

/**
 * Prepares variables for the StoryMapJS style template.
 */
function template_preprocess_views_storymapjs(&$vars) {
  $library_location = variable_get('views_storymapjs_library', 'cdn');
  $view = $vars['view'];
  $args = '';

  if (!empty($view->args)) {
    $args = implode($view->args);
  }
  $id = drupal_html_id('storymapjs_' . $view->name . '_' . $view->current_display.'_'.$args);
  $style = 'width: ' . $vars['storymap_options']['width'] . '; height: ' .  $vars['storymap_options']['height'] . ';';
  $vars['div_attributes'] = drupal_attributes(array('id' => $id, 'style' => $style, 'class' => 'storymapjs'));

  // Load required libraries and styles.
  $library = drupal_add_library('views_storymapjs', 'storymapjs.' . $library_location);

  // Add inline JavaScript.
  $settings = array(
    'embed_id' => $id,
    'map_object' => _sanitize_storymap_object($vars['storymap_options'], $vars['slides']),
    'processed' => FALSE,
  );
  drupal_add_js(array('storymapJS' => array($settings)), 'setting');
  drupal_add_js(drupal_get_path('module', 'views_storymapjs') . '/js/views_storymapjs.js', array('scope' => 'footer'));
}

/**
 * Sanitizes the storymap options.
 */
function _sanitize_storymap_object($options, $slides) {
  // Sanitize the options.
  $options = array_map('check_plain', $options);
  // Remove empty values from the options before returning.
  $options = array_filter($options);
  $object = array(
    'width' => $options['width'],
    'height' => $options['height'],
    'calculate_zoom' => !empty($options['calculate_zoom']) ? TRUE : FALSE,
    'storymap' => array(
      'language' => $options['language'],
      'map_type' => $options['map_type'],
      'map_as_image' => FALSE,
      'slides' => $slides,
    ),
  );
  return $object;
}
