Views StoryMapJS
================
This module borrow most of code from Views TimelineJS integration https://www.drupal.org/project/views_timelinejs

This module adds a new style plugin for Views which renders result rows as StoryMapJS slides. For more information about StoryMapJS visit storymap.knightlab.com or the GitHub repository https://github.com/NUKnightLab/StoryMapJS.

