/**
 * @file
 * Invokes the storymap js plugin.
 */

(function ($) {
  Drupal.behaviors.storymapJS = {
    attach: function(context, settings) {
      $.each(Drupal.settings.storymapJS, function(key, storymap) {
        if (storymap['processed'] != true) {
          var opt = {};
          // console.log(storymap['map_object']);
          window.storymap = new VCO.StoryMap(storymap['embed_id'], storymap['map_object']);
        }
        storymap['processed'] = true;
      });
    }
  }
})(jQuery);
