<?php

/**
 * Style plugin to render items as storymapjs3 slides.
 *
 * @ingroup views_style_plugins
 */
class views_storymapjs_plugin_style_storymapjs extends views_plugin_style {

  /**
   * The row index of the slide at which the storymap should first be rendered.
   */
  protected $start_slide_index;

  /** 
   * The object where generate storymap data json
   */
  public $storymap_data;

  /**
   * {@inheritdoc}
   */
  function option_definition() {
    $options = parent::option_definition();

    $options['storymap_config'] = array(
      'contains' => array(
        'width' => array('default' => '100%'),
        'height' => array('default' => '800px'),
        'calculate_zoom' => array('default' => true),
        'language' => array('default' => ''),
        'map_type' => array('default' => 'osm:standard'),
      ),
    );
    $options['storymap_fields'] = array(
      'contains' => array(
        'type' => array('default' => ''),
        'lat' => array('default' => ''),
        'lon' => array('default' => ''),
        'headline' => array('default' => ''),
        'text' => array('default' => ''),
        'url' => array('default' => ''),
        'caption' => array('default' => ''),
        'credit' => array('default' => ''),
      ),
    );
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  function options_form(&$form, &$form_state) {
    $initial_labels = array('' => t('- None -'));
    $view_fields_labels = $this->display->handler->get_field_labels();
    $view_fields_labels = array_merge($initial_labels, $view_fields_labels);

    // storymap general configuration.  Values within this fieldset will be
    // passed directly to the storymapjs settings object.  As a result, form
    // element keys should be given the same ID as storymapjs settings, e.g.
    // $form['storymap_config']['id_of_storymapjs_option'].  See the list of
    // options at https://storymap.knightlab.com/docs/options.html.
    $form['storymap_config'] = array(
      '#type' => 'fieldset',
      '#title' => t('storymapjs Options'),
      '#description' => t('Each of these settings maps directly to one of the storymapjs presentation options.  See the <a href="@options-doc">options documentation page</a> for additional information.', array('@options-doc' => 'https://storymap.knightlab.com/docs/options.html')),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );
    $form['storymap_config']['width'] = array(
      '#type' => 'textfield',
      '#title' => t('storymap width'),
      '#description' => t('The width of the storymap, e.g. "100%" or "650px".'),
      '#default_value' => $this->options['storymap_config']['width'],
      '#size' => 10,
      '#maxlength' => 10,
    );
    $form['storymap_config']['height'] = array(
      '#type' => 'textfield',
      '#title' => t('storymap height'),
      '#description' => t('The height of the storymap, e.g. "40em" or "650px".  Percent values are not recommended for the height.'),
      '#default_value' => $this->options['storymap_config']['height'],
      '#size' => 10,
      '#maxlength' => 10,
    );
    $form['storymap_config']['language'] = array(
      '#type' => 'select',
      '#title' => t('Language'),
      '#description' => t("By default, the storymap will be displayed in the site's current language if it is supported by storymapjs. Selecting a language in this setting will force the storymap to always be displayed in the chosen language."),
      '#options' => array_merge($initial_labels, _views_storymapjs_list_languages()),
      '#default_value' => $this->options['storymap_config']['language'],
    );
    $form['storymap_config']['map_type'] = array(
      '#type' => 'select',
      '#title' => t('Map Type'),
      '#options' => _views_storymapjs_list_maptype(),
      '#default_value' => $this->options['storymap_config']['map_type'],
    );
    $form['storymap_config']['map_url'] = array(
      '#type' => 'textfield',
      '#title' => t('Map Type'),
      '#description' => t('Custom URL tile template as described in the Leaflet documentation'),
      '#default_value' => $this->options['storymap_config']['map_url'],
    );

    // Field mapping.
    $form['storymap_fields'] = array(
      '#type' => 'fieldset',
      '#title' => t('Field mappings'),
      '#description' => t('Map your Views data fields to storymapjs slide object properties.'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );
    $form['storymap_fields']['type'] = array(
      '#type' => 'select',
      '#options' => $view_fields_labels,
      '#title' => t('Type'),
      '#description' => t('First slide will be "overview", otherwise will be none.'),
      '#default_value' => $this->options['storymap_fields']['type'],
    );
    $form['storymap_fields']['lat'] = array(
      '#type' => 'select',
      '#options' => $view_fields_labels,
      '#title' => t('Latitude'),
      '#default_value' => $this->options['storymap_fields']['lat'],
    );
    $form['storymap_fields']['lon'] = array(
      '#type' => 'select',
      '#options' => $view_fields_labels,
      '#title' => t('Longitude'),
      '#default_value' => $this->options['storymap_fields']['lon'],
    );
    $form['storymap_fields']['headline'] = array(
      '#type' => 'select',
      '#options' => $view_fields_labels,
      '#title' => t('Headline'),
      '#description' => t('The selected field may contain any text, including HTML markup.'),
      '#default_value' => $this->options['storymap_fields']['headline'],
    );
    $form['storymap_fields']['text'] = array(
      '#type' => 'select',
      '#options' => $view_fields_labels,
      '#title' => t('Body text'),
      '#description' => t('The selected field may contain any text, including HTML markup.'),
      '#default_value' => $this->options['storymap_fields']['text'],
    );
    $form['storymap_fields']['url'] = array(
      '#type' => 'select',
      '#options' => $view_fields_labels,
      '#title' => t('Media URL'),
      '#default_value' => $this->options['storymap_fields']['url'],
    );
    $form['storymap_fields']['caption'] = array(
      '#type' => 'select',
      '#title' => t('Media Caption'),
      '#description' => t('The selected field may contain any text, including HTML markup.'),
      '#options' => $view_fields_labels,
      '#default_value' => $this->options['storymap_fields']['caption'],
    );
    $form['storymap_fields']['credit'] = array(
      '#type' => 'select',
      '#title' => t('Media Credit'),
      '#description' => t('The selected field may contain any text, including HTML markup.'),
      '#options' => $view_fields_labels,
      '#default_value' => $this->options['storymap_fields']['credit'],
    );
    $form['storymap_fields']['map_type_field'] = array(
      '#type' => 'select',
      '#title' => t('Media Type Field'),
      '#description' => t('The selected field may contain any text, including HTML markup.'),
      '#options' => $view_fields_labels,
      '#default_value' => $this->options['storymap_fields']['map_type_field'],
    );
    $form['storymap_fields']['marker'] = array(
      '#type' => 'select',
      '#title' => t('Marker icon'),
      '#description' => t('Custom icon.'),
      '#options' => $view_fields_labels,
      '#default_value' => $this->options['storymap_fields']['marker'],
    );
  }

  /**
   * {@inheritdoc}
   */
  function render() {
    // Render the fields.  If it isn't done now then the row_index will be unset
    // the first time that get_field() is called, resulting in an undefined
    // property exception.

    
    $this->render_fields($this->view->result);

    // Render slide arrays from the views data.
    $has_overview = FALSE;
    $map_type_field = FALSE;
    foreach ($this->view->result as $row_index => $row) {
      $this->view->row_index = $row_index;
      if (!isset($first_slide)) {
        $first_slide = $row_index;
      }

      // Determine the type of storymap entity to build.
      $type = '';
      if ($this->options['storymap_fields']['type']) {
        $type = $this->get_field($row_index, $this->options['storymap_fields']['type']);
      }
      switch ($type) {
        case 'overview':
          $has_overview = TRUE;
          $this->build_title_slide();
          break;
        default:
          $this->build_slide();
          break;
      }
    }

    // automatic set first row to overview slide
    if (!$has_overview && isset($first_slide)) {
      $this->storymap_data[$first_slide]['type'] = 'overview';
      // unset location to let overview calculate it's own location
      unset($this->storymap_data[$first_slide]['location']);
    }

    // use first result to handling map_type or url
    // this will reset row_index of view
    if (!empty($this->options['storymap_fields']['map_type_field'])) {
      $map_type_field_value = $this->get_field_value(0, $this->options['storymap_fields']['map_type_field']);
      $map_type_field = !empty($map_type_field_value[0]['value']) ? $map_type_field_value[0]['value'] : FALSE;
    }
    unset($this->view->row_index);

    if ($map_type_field) {
      $this->options['storymap_config']['map_type'] = $map_type_field;
    }
    elseif (!empty($this->options['storymap_config']['map_url'])) {
      if (preg_match('/^mapbox/i', $this->options['storymap_config']['map_url'])) {
        $this->options['storymap_config']['map_type'] = $this->options['storymap_config']['map_url'];
      }
      else if (preg_match('/^http:\/\//i', $this->options['storymap_config']['map_url'])) {
        $this->options['storymap_config']['map_type'] = $this->options['storymap_config']['map_url'];
      }
    }

    // Skip theming if the view is being edited or previewed.
    if ($this->view->editing) {
      return '<pre>' . htmlspecialchars(json_encode($this->storymap_data, JSON_PRETTY_PRINT)) . '</pre>';
    }

    // Prepare the options array.
    $this->prepare_storymap_options();

    return theme('views_storymapjs', array(
      'view' => $this->view,
      'storymap_options' => $this->options['storymap_config'],
      'slides' => $this->storymap_data,
    ));
  }

  /**
   * Builds a storymap slide from the current views data row.
   *
   * @return storymapSlideInterface|NULL
   *   A slide object or NULL if the start date could not be parsed.
   */
  protected function build_slide() {
    $this->storymap_data[$this->view->row_index]['type'] = '';
    $this->build_text();
    $this->build_media();
    $this->build_location();
  }

  /**
   * Builds a storymap title slide from the current views data row.
   *
   * @return storymapSlideInterface
   *   A slide object.
   */
  protected function build_title_slide() {
    $this->storymap_data[$this->view->row_index]['type'] = 'overview';
    $this->build_text();
    $this->build_media();
  //  $this->build_location();
  }

  /**
   * Builds storymap text from the current data row.
   *
   * @return NULL
   */
  protected function build_text() {
    $headline = '';
    if (!empty($this->options['storymap_fields']['headline'])) {
      $headline = $this->get_field($this->view->row_index, $this->options['storymap_fields']['headline']);
    }

    $text = '';
    if (!empty($this->options['storymap_fields']['text'])) {
      $text = $this->get_field($this->view->row_index, $this->options['storymap_fields']['text']);
    }

    $this->storymap_data[$this->view->row_index]['text'] = array(
      'headline' => $headline,
      'text' => $text,
    );
  }

  /**
   * Builds storymap media from the current data row.
   *
   * @return NULL
   */
  protected function build_media() {
    $url = '';
    if (!empty($this->options['storymap_fields']['url'])) {
      $url = $this->get_field($this->view->row_index, $this->options['storymap_fields']['url']);

      // Special handling because core Image fields have no raw URL formatter.
      // Check to see if we don't have a raw URL.
      if (!filter_var($url, FILTER_VALIDATE_URL)) {
        // Attempt to extract a URL from an img or anchor tag in the string.
        $url = $this->extract_url($url);
      }
    }
    // Return NULL if the URL is empty.
    if (empty($url)) {
      return NULL;
    }

    $caption = '';
    if (!empty($this->options['storymap_fields']['caption'])) {
      $caption = $this->get_field($this->view->row_index, $this->options['storymap_fields']['caption']);
    }
    $credit = '';
    if (!empty($this->options['storymap_fields']['credit'])) {
      $credit = $this->get_field($this->view->row_index, $this->options['storymap_fields']['credit']);
    }
    $this->storymap_data[$this->view->row_index]['media'] = array(
      'url' => $url,
      'caption' => $caption,
      'credit' => $credit,
    );
  }
  /**
   * Builds storymap location from the current data row.
   *
   * @return NULL
   */
  protected function build_location() {
    $lat = '';
    if (!empty($this->options['storymap_fields']['lat'])) {
      $lat = $this->get_field($this->view->row_index, $this->options['storymap_fields']['lat']);
    }
    $lon = '';
    if (!empty($this->options['storymap_fields']['lon'])) {
      $lon = $this->get_field($this->view->row_index, $this->options['storymap_fields']['lon']);
    }
    if ($lat && $lon) {
      $this->storymap_data[$this->view->row_index]['location'] = array(
        'lat' => (float) $lat,
        'lon' => (float) $lon,
      );
    }
    if (!empty($this->options['storymap_fields']['marker'])) {
      $marker_image = $this->get_field($this->view->row_index, $this->options['storymap_fields']['marker']);
      if ($marker_image) {
        $this->storymap_data[$this->view->row_index]['location']['image'] = $marker_image;
      }
    }
  }

  /**
   * Searches a string for HTML attributes that contain URLs and returns them.
   *
   * This will search a string which is presumed to contain HTML for anchor or
   * image tags.  It will return the href or src attribute of the first one it
   * finds.
   *
   * This is basically special handling for core Image fields.  There is no
   * built-in field formatter for outputting a raw URL from an image.  This
   * method allows image fields to "just work" as sources for storymapjs media
   * and background image URLs.  Anchor tag handling was added for people who
   * forget to output link fields as plain text URLs.
   *
   * @param string $html
   *   A string that contains HTML.
   *
   * @return string
   *   A URL if one was found in the input string, the original string if not.
   */
  protected function extract_url($html) {
    if (!empty($html)) {
      // Disable libxml errors.
      $previous_use_errors = libxml_use_internal_errors(TRUE);

      $document = new DOMDocument();
      $document->loadHTML($html);

      // Handle XML errors.
      foreach (libxml_get_errors() as $error) {
        $this->handle_xml_errors($error, $html);
      }

      // Restore the previous error setting.
      libxml_use_internal_errors($previous_use_errors);

      // Check for anchor tags.
      $anchor_tags = $document->getElementsByTagName('a');
      if ($anchor_tags->length) {
        return $anchor_tags->item(0)->getAttribute('href');
      }

      // Check for image tags.
      $image_tags = $document->getElementsByTagName('img');
      if ($image_tags->length) {
        return $image_tags->item(0)->getAttribute('src');
      }
    }
    return $html;
  }

  /**
   * Sets Drupal messages to inform users of HTML parsing errors.
   *
   * @param \LibXMLError $error
   *   Contains information about the XML parsing error.
   * @param type $html
   *   Contains the original HTML that was parsed.
   */
  protected function handle_xml_errors(\LibXMLError $error, $html) {
    $message = t('A media field has an error in its HTML.<br>Error message: @message<br>Views result row: @row<br>HTML: <pre>@html</pre>', array('@message' => $error->message, '@row' => $this->view->row_index, '@html' => $html));
    drupal_set_message($message, 'warning');
  }

  /**
   * Processes storymap options before theming.
   */
  protected function prepare_storymap_options() {
    // Set the language option to the site's default if it is empty.
    if (empty($this->options['storymap_config']['language'])) {
      $this->prepare_language_option();
    }
  }

  /**
   * Sets the storymap language option to the site's current language.
   */
  protected function prepare_language_option() {
    global $language;
    $supported_languages = _views_storymapjs_list_languages();
    $language_map = _views_storymapjs_language_map();

    // Check for the site's current language in the list of languages that are
    // supported by storymapjs.
    if (isset($supported_languages[$language->language])) {
      $this->options['storymap_config']['language'] = $language->language;
    }
    // Check for the site's current language in the list of language codes
    // that are different in Drupal than they are in storymapjs.
    elseif (isset($language_map[$language->language])) {
      $this->options['storymap_config']['language'] = $language_map[$language->language];
    }
  }

}
